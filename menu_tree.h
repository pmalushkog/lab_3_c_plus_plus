#ifndef LAB_3_MENU_TREE_H
#define LAB_3_MENU_TREE_H

#include <iostream>
#include <complex>
#include "BinaryTree.h"
#include "Set.h"
#include<algorithm>
#include <iomanip>
#include <string>
#include<Windows.h>

using namespace std;

template <typename T>
void inputTree(int size, bTree<T> &tree){
    T x = 0;
    for (int i = 0; i < size; i ++) {
        cin >> x;
        tree.insert(x);
    }
}

template <typename T>
void populateTree(int size, bTree<T> &tree){

    for (int i = 0; i < size; i++){
        T value = rand() % 100 + 1;
        tree.insert(value);
    }
}

template <typename Type>
int menu_Binary_Tree(){
    SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);

    int operation = 1;
    while (1) {

        int size;
        int size_sub;
        cout << "Возможные действия:\n";
        cout<< "1. Сделать балансировку\n2. Найти поддерево\n3. Показать дерево\n4. Случайное сбалансированное дерево\n5. Назад"<< endl;
        cin >> operation;

        if (operation == 5) {
            return 0;
        }

        cout << "Введите размер дерева:\n";
        cin >> size;

        bTree<Type> a;
        bTree<Type> b;

        if (operation == 1) {
            cout << "Введите значения дерева:\n";
            inputTree(size, a);
            a.balance();
            cout << "\nСбалансироавнное двоичное дерево:\n";
            cout << "--------------------------------------------------------------------------------" << endl;
            a.printInorder(a.getRoot(), 10);
            cout << "--------------------------------------------------------------------------------" << endl;
            break;
        }
        else if (operation == 2) {
            cout << "Введите значения дерева:\n";
            inputTree(size, a);
            cout << "Введите размер поддерева:\n";
            cin >> size_sub;
            cout << "Введите значения поддерева:\n";
            inputTree(size, b);

            cout << "Найдено поддерево:\n";
            cout << "--------------------------------------------------------------------------------" << endl;
            b.printInorder(b.getRoot(), 10);
            cout << "--------------------------------------------------------------------------------" << endl;

            if (a.findSubTree(b)) {
                cout << "В вашем дереве есть такое поддерево\n";
            } else {
                cout << "В дереве нет такого поддерева\n";
            }
        }
        else if (operation == 3) {
            cout << "Введите значения дерева:\n";
            inputTree(size, a);
            cout << "--------------------------------------------------------------------------------" << endl;
            a.printInorder(a.getRoot(), 10);
            cout << "--------------------------------------------------------------------------------" << endl;

        } else if (operation == 4) {
            populateTree(size, a);
            a.balance();
            cout << "Случайное сбалансированное дерево:\n";
            cout << "--------------------------------------------------------------------------------" << endl;
            a.printInorder(a.getRoot(), 10);
            cout << "--------------------------------------------------------------------------------" << endl;

        }
    }

}

#endif //LAB_3_MENU_TREE_H
