#ifndef LAB_3_BINARYTREE_H
#define LAB_3_BINARYTREE_H
#include <iostream>
#include <iomanip>
#include <vector>   // push_back - добавляет в конец массива элемент
#include <queue>
#include <string>

template<class T> class bTree;   // Вводм в зону видимости bTree

template<class Type>
class Node {
    friend class bTree<Type>;

protected:
    Node *left;
    Node *right;
    Node *parent;
    Type data;

public:
    explicit Node(const Type &n) {
        data = n;
        left = 0;
        right = 0;
    }

    Type getData() {
        return data;
    }
};

template<class T>
class bTree {
private:
    Node<T> *root;
    size_t size;
public:
    bTree() {
        size = 0;
        root = nullptr;
    }

    void insert(const T &el)  // Функция вставки элемента в дерево x = el; n = num; ptr1 = prt_tmp;
    {
        auto* num = new Node<T>(el);
        Node<T>* ptr;
        Node<T>* ptr_tmp = nullptr;

        num->parent = nullptr;
        num->left = nullptr;
        num->right = nullptr;
        ptr = root;
        while(ptr != nullptr)  // Пока дерево не закончилось
        {
            ptr_tmp = ptr;
            if(el < ptr->data )
                ptr = ptr->left;
            else
                ptr = ptr->right;
        }
        num->parent = ptr_tmp;
        if(ptr_tmp == 0)
            root = num;
        else
        {
            if(el < ptr_tmp->data )
                ptr_tmp->left = num;
            else
                ptr_tmp->right = num;
        }
        size++;
    }
    Node<T>* findNode(Node<T>* num, const T &value)   // Рекурсивно ищем элемент в дереве
    {
        if(num == nullptr or value == num->getData())
            return num;
        if(value > num->getData() )
            return findNode(num->right, value);
        else
            return findNode(num->left, value);
    }
    Node<T>* findMin(Node<T>* a)   // Ищем наимеьший элемент (заменить x)
    {
        while(a->left != 0)
            a = a->left;
        return a;
    }

    Node<T>* findMax(Node<T>* a) // Ищем максимальный элемент (заменить x)
    {
        while(a->right != 0)
            a = a->right;
        return a;
    }

    Node<T>* findNext(const T & val)   // Функция (заменить x = a; и findNext от findGlobalMax)
    {
        Node<T>* a = findNode(root, val);
        Node<T>* b;
        if(a == 0)
            return 0;
        if(a->right != 0)
            return findMin(a->right);
        b = a->parent;
        while(b != 0 && a == b->right)
        {
            a = b;
            b = b->parent;
        }
        return b;
    }

    Node<T>* deleteNode(Node<T> *z)
    {
        Node<T>* y;
        Node<T>* x;
        if(z->left == 0 or z->right == 0)
            y = z;
        else
            y = findNext(z->getData());
        if(y->left!=0)
            x = y->left;
        else
            x = y->right;
        if(x!=0)
            x->parent = y->parent;
        if(y->parent == 0)
            root = x;
        else
        {
            if (y == (y->parent)->left)
                (y->parent)->left = x;
            else
                (y->parent)->right = x;
        }
        if(y != z)
            z->data = y->getData();
        size--;
        return y;
    }


    void storeNodes(Node<T> *r, std::vector<Node<T> *> &nodes) {
        if (r == nullptr)
            return;
        storeNodes(r->left, nodes);
        nodes.push_back(r);
        storeNodes(r->right, nodes);
    }

    std::vector<T> get2D()
    {
        std::vector<Node<T>*> nodes;
        std::vector<T> res;
        storeNodes(root, nodes);
        for (size_t i = 0; i < nodes.size(); i++){
            res.push_back(nodes[i]->getData());
        }
        return res;
    }

    Node<T>* allocationNodes(std::vector<Node<T>*> &nodes, int start, int end)   // Делает бинарное дерево из массива который выходит из storeNodes
    {

    if (start > end)
        return nullptr;

    int mid = (start + end) / 2;   // Определяем текущую вершину от которой будем дальше заполнять
    auto *r = nodes[mid];   // Записывает значения mid

    r->left  = allocationNodes(nodes, start, mid - 1);   // Рекурсия
    r->right = allocationNodes(nodes, mid + 1, end);

    return r;
    }

        void balance() {   // сначала переводит в массив потом вывзвает allocationNodes (Делает левую и правую части дерева одинаковые по размеру)
        auto *rt = root;

        std::vector<Node<T>*> nodes;
        storeNodes(rt, nodes);
        auto n = nodes.size();
        rt = allocationNodes(nodes, 0, n - 1);

        root = rt;
    }

    bool search(const T &value) {   // Посик в дереве (проверяет есть ли этот элемент в дереве)

        auto *current = getRoot();
        while (current){

            if (value > current->getData()){
                current = current->right;
            } else if ( value < current->getData() ) {
                current = current->left;
            } else if ( value == current->getData()){
                return true;
            }

        }

        return false;
    }

    bool isEquals(Node<T> * a, Node<T> * b ) {  // Проверяет два дерева на идентичность

        if (a == nullptr and b == nullptr ){
            return true;
        }
        if (a == nullptr or b == nullptr){
            return false;
        }

        if (a->getData() != b->getData()){
            return false;
        }

        return isEquals(a->left, b->left) and isEquals(a->right, b->right);
    }


    bool inOrderCompare(Node<T> *a, Node<T> *b ) { // Проверяет есть ли такое поддерево в a что это  b


        return isEquals(a, b)
               or (a->left != nullptr and inOrderCompare(a->left, b))
               or (a->right != nullptr and inOrderCompare(a->right, b));
    }

    bool findSubTree(bTree<T> tree) {  // То же самое что и inOrdercompare только чтобы удобнее было вызывать

        return inOrderCompare(root, tree.getRoot());
    }

    void mapper(T (*func)(T), Node<T> *n) {   // Применяет какую то функцию ко всем элементам дерева
        n->data = func(n->getData());
        if (n->left) {
            mapper(func, n->left);
        }
        if ( n->right) {
            mapper(func, n->right);
        }
    }

    bTree<T> map(T (*func)(T)) {
        if (func == nullptr){
            return *this;
        }
        bTree<T> res(getRoot());
        mapper(func, res.getRoot());
        return res;
    }

    void wPath(bool (*predicate)(T), Node<T> * n , bTree<T> *tree) {   // Функция которая применяет where
        if (predicate(n->getData())){
            tree->insert(n->getData());
        }
        if(n->left){
            wPath(predicate, n->left, tree);
        }
        if (n->right){
            wPath(predicate, n->right, tree);
        }
    }


    bTree<T> where(bool (*predicate)(T)) {   // Создает дерево из значений которвые нас удвлетворяют
        if (predicate == nullptr){
            return *this;
        }
        bTree<T> res;
        wPath(predicate, getRoot(), &res);
        return res;
    }

    T reduce(T (*reducer)(T, T), const T &value) {   // Функция свертки, передаем туда функцию reducer(напр. +)
        if (reducer == nullptr){
            return 0;
        }
        T res = reducePath(reducer, getRoot(), value);
        return res;
    }


T reducePath(T (*reducer)(T, T), Node<T> *n, const T &value) {   // Использует нашу фукнцию reducer для Node
    if (n) {
        T res = reducer(n->data, value);
        res = reducePath(reducer, n->left, res);
        res = reducePath(reducer, n->right, res);
        return res;
    }

    return value;
}

int getSize() {
    return size;
}


    void printInorder(Node<T>* n, size_t space){

    if(n != 0)
    {
        space += 10;
        printInorder(n->right, space);

        int temp = 1;

        std::cout << std::endl;
        for (size_t i = 10; i < space + 1; i++){
            std::cout<<" ";
        }
        std::cout << n->getData() << std::endl;
        //printInorder(n->right, space,1);

        printInorder(n->left, space);
        //printInorder(n->left, space,0);

        temp = 0;

    }
}

    Node<T>* getRoot()
    {
        return root;
    }










};



















#endif //LAB_3_BINARYTREE_H
