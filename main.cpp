#include <iostream>
#include<Windows.h>
#include"menu_tree.h"
#include"menu_set.h"
using namespace std;


int main() {
    SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);
    unsigned type = 1;
    int ts = 1;
    while(1) {

        cout << "Выберите то, с чем хотите работать:\n 1. Бинарное дерево\n 2. Множество\n\n 3. Выйти из программы" << endl;
        cin >> ts;

        if (ts == 1) {
            cout << "Выберите тип значений в дереве:\n";
            cout << "1. Целое число\n2. Нецелое число\n\n 3. Выход" << endl;
            cin >> type;
            if (type == 1){
                menu_Binary_Tree<int>();
            }
            else if (type == 2) {
                menu_Binary_Tree<float>();
            }

            else{
                cout << "Ошибка выбора типа" << endl;
            }

            }
        else if (ts == 2) {

            cout << "Выберите тип значений в множестве:\n";
            cout << "1. Целое число\n2. Нецелое число\n\n 3. Выход" << endl;
            cin >> type;

            if (type == 1) {
                menuSet<int>();
            }
            if (type == 2) {
                menuSet<float>();
            }
        }
        else if (ts == 3){
            cout << "Выход из программы..." << endl;
            break;
        } else {
            cout << "Ошибка выбора типа\n" << endl;
        }

    }

return 0;
}







