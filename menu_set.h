#ifndef LAB_3_MENU_SET_H
#define LAB_3_MENU_SET_H

#include <iostream>
#include <complex>
#include "BinaryTree.h"
#include "Set.h"
#include<algorithm>
#include <iomanip>
#include <string>
#include<Windows.h>
#include"menu_tree.h"
using namespace std;

template <typename T>
void inputSet(int size, set<T> &st){
    T x;
    for (int i = 0; i < size; i ++) {
        cin >> x;
        st.add(x);
    }
}

template <typename Type>
int menuSet(){
    SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);

    int operation = 1;

    while (1){
        int size_1;
        int size_2;

        set<Type> a;
        set<Type> b;

        cout << "\nВозможные действия:\n";
        cout << "1. Объединение\n2. Пересечение\n3. Вычитание\n4. Эквивалентность двух множеств\n\n5. Выход" << endl;
        cin >> operation;

        if  (operation == 5) {
            return 0;
        }



        cout << "Какого размера будет 1-е множество?\n";
        cin >>  size_1;
        cout << "Какого размера будет 2-е множество?\n";
        cin >>  size_2;


        if (operation == 1){

            cout << "Введите первое множество:\n";
            inputSet(size_1, a);
            cout << "Введите второе множество:\n";
            inputSet(size_2, b);
            a.unionSet(b);
            cout << "Результат объединения: \n";
            a.print();
            cout << endl;
        } else if (operation == 2) {
            cout << "Введите первое множество:\n";
            inputSet(size_1, a);
            cout << "Введите второе множество:\n";
            inputSet(size_2, b);
            a.intersection(b);
            cout << "Результат пересечения: \n";
            a.print();
            cout << endl;
        } else if (operation == 3){
            cout << "Введите первое множество:\n";
            inputSet(size_1, a);
            cout << "Введите второе множество:\n";
            inputSet(size_2, b);
            a.difference(b);
            cout << "Результат вычитания:\n";
            a.print();
        } else if (operation == 4) {
            cout << "Введите первое множество:\n";
            inputSet(size_1, a);
            cout << "Введите второе множество:\n";
            inputSet(size_2, b);
            if ((a == b) and (size_1 == size_2)){
                cout << "Они эквивалентны\n";
            } else {
                cout << "Они не эквивалентны\n";
            }
        }



        else {
            cout << "Ошибка выбора операции \n";
        }


    }
}



#endif //LAB_3_MENU_SET_H
